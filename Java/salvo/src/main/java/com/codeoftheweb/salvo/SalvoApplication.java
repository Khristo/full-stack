package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Date;


@SpringBootApplication
public class SalvoApplication {

  public static void main(String[] args) {
    SpringApplication.run(SalvoApplication.class, args);
    System.out.println("SALVO RUNNING");
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }

  @Bean
  public CommandLineRunner initData(PlayerRepository playerRepository,
                                    GameRepository gameRepository,
                                    GamePlayerRepository  gamePlayerRepository,
                                    ShipRepository  shipRepository,
                                    SalvoRepository salvoRepository,
                                    ScoreRepository scoreRepository) {
    return (args) -> {
// Creacion de usuarios
      Player player1  = new Player("christian_admin@gmail.com",passwordEncoder().encode("adminQWERTY"));
      Player  player2  = new Player("opponent_enemy@gmail.com",passwordEncoder().encode("oppo1234"));
      playerRepository.save(player1);
      playerRepository.save(player2);
//Creacion de juegos con horarios
      Game game1 = new Game();
      Game game2 = new Game();
      game2.setCreated(Date.from(game1.getCreated().toInstant().plusSeconds(3600)));
      Game game3 = new Game();
      game3.setCreated(Date.from(game1.getCreated().toInstant().plusSeconds(7200)));
      gameRepository.save(game1);
      gameRepository.save(game2);
      gameRepository.save(game3);
// Relacion de juegos con jugadores
      GamePlayer  gamePlayer1 = new GamePlayer(game1,player1);
      GamePlayer  gamePlayer2 = new GamePlayer(game1,player2);
      GamePlayer  gamePlayer3 = new GamePlayer(game2,player2);
      GamePlayer  gamePlayer4 = new GamePlayer(game2,player1);
      gamePlayerRepository.save(gamePlayer1);
      gamePlayerRepository.save(gamePlayer2);
      gamePlayerRepository.save(gamePlayer3);
      gamePlayerRepository.save(gamePlayer4);
// Creacion de botes con ubicacion
      String galeónII = "galeón-II";
      String galeónIII = "galeón-III";
      String galeónIV = "galeón-IV";
      String galeónV = "galeón-V";
      Ship ship1 = new Ship(galeónIV, Arrays.asList("H2", "H3", "H4"),gamePlayer1);
      Ship ship2 = new Ship(galeónII, Arrays.asList("E1", "F1", "G1"),gamePlayer1);
      Ship ship3 = new Ship(galeónV, Arrays.asList("B4", "B5"),gamePlayer1);
      Ship ship4 = new Ship(galeónIV, Arrays.asList("B5", "C5", "D5"),gamePlayer2);
      Ship ship5 = new Ship(galeónV, Arrays.asList("F1", "F2"),gamePlayer2);
      shipRepository.save(ship1);
      shipRepository.save(ship2);
      shipRepository.save(ship3);
      shipRepository.save(ship4);
      shipRepository.save(ship5);
// Ubicacion de disparos
      Salvo salvo1 = new Salvo(1,Arrays.asList("H2", "H3", "H4"),gamePlayer1);
      Salvo salvo2 = new Salvo(1,Arrays.asList("E1", "F1", "G1"),gamePlayer2);
      salvoRepository.save(salvo1);
      salvoRepository.save(salvo2);
// Creacion de Puntaje
      Score score1 = new Score(player1,game1,1.0D,new Date());
      Score score2 = new Score(player2,game1,0.0D,new Date());
      scoreRepository.save(score1);
      scoreRepository.save(score2);
    };
  }
}
// Seguridad Web login logout
@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

  @Autowired
  PlayerRepository playerRepository;

  @Override
  public void init(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(inputName-> {
      Player player = playerRepository.findByEmail(inputName).get();
      if (player != null) {
        return new User(player.getEmail(), player.getPassword(),
                AuthorityUtils.createAuthorityList("USER"));
      } else {
        throw new UsernameNotFoundException("Unknown user: " + inputName);
      }
    });
  }
}

@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
            .antMatchers("/web/**").permitAll()
            .antMatchers("/api/game_view/*").hasAuthority("USER")
            .antMatchers("/h2-console/**").permitAll()
            .antMatchers("/api/games").permitAll();

    http.formLogin()
            .usernameParameter("name")
            .passwordParameter("pwd")
            .loginPage("/api/login");

    http.logout().logoutUrl("/api/logout");

    http.csrf().disable();
    http.headers().frameOptions().disable();

    http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

    http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

    http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

    http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
  }

  private void clearAuthenticationAttributes(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    if (session != null) {
      session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
  }
}

