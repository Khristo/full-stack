function glanceTable(idTable){
    table = document.getElementById(idTable);
    tblBody = document.createElement("tbody");

    for(i = 0; i < stadistics.resultados[0].partido.length; i++){
        tr = document.createElement("tr");
        party = stadistics.resultados[0].partido[i].party
        totalMembers =  stadistics.resultados[0].partido[i].miembros.length;
        pctParty = stadistics.resultados[0].partido[i].pct_votos;

        if(pctParty == "NaN"){
            pctParty = 0;
        }

        partyNode = document.createTextNode(party);
        totalMembersNode = document.createTextNode(totalMembers);
        pctPartyNode = document.createTextNode(pctParty);

        td = document.createElement("td");
        td.appendChild(partyNode);
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(totalMembersNode);
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(pctPartyNode);
        tr.appendChild(td);
        tblBody.appendChild(tr);
    }
    table.appendChild(tblBody);
}
glanceTable("glance-table");
