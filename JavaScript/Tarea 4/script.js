var leastLoyalApp = new Vue({
  el: "#least-loyal",
  data: {
    members:"",
    headers:""
  }
});

var mostLoyalApp = new Vue({
  el: "#most-loyal",
  data: {
    members:"",
    headers:""
  }
});

var leastEngagedApp = new Vue({
  el: "#least-engaged",
  data: {
    members:"",
    headers:""
  }
});

var mostEngagedApp = new Vue({
  el: "#most-engaged",
  data: {
    members:"",
    headers:""
  }
});

var loyaltyHeaders = ["Name","No. Party Votes","% Party Votes"];

var loyaltyKeys = ["name","votes_with_party","votes_with_party_pct"];

var loyaltyJSON = {
  "first_name":"",
  "last_name":"",
  "middle_name":"",
  "party":"",
  "total_votes":"",
  "votes_with_party_pct":""
}

var attendanceHeaders = ["Name","No. Missed Votes","% Missed Votes"];

var attendanceKeys = ["name","missed_votes","missed_votes_pct"];

var attendanceJSON = {
  "first_name":"",
  "last_name":"",
  "middle_name":"",
  "party":"",
  "missed_votes":"",
  "missed_votes_pct":""
}

function updateLoyaltyData (members) {
  for (let i=0;i<members.length;i++) {
    members[i].votes_with_party=Math.round(members[i].total_votes*members[i].votes_with_party_pct/100);
    members[i].votes_with_party_pct+=" %";
    delete members[i].total_votes;
  }
  return members;
}

function createSummaryData () {
	let tableString="";
	for (let i=0; i<statistics.parties.length; i++) {
      tableString+="<tr>";		
      tableString+="<td>";
      tableString+=statistics.parties[i].name;
      tableString+="</td>";		
      tableString+="<td>";
      tableString+=statistics.parties[i].number_of_members;
      tableString+="</td>";		
      tableString+="<td>";
      tableString+=statistics.parties[i].perc_voted_with_party;
      tableString+=" %";
      tableString+="</td>";
      tableString+="</tr>";
	}

	return tableString;
}

function createSummaryHeaders () {
  let titles = ["Party","No. of Reps.","% Voted w/ Party"];
  return createHeaders(titles);
}

function createHeaders (titles) {
  let headers="";
  for (let i=0;i<titles.length;i++) {
    headers+="<th scope=\"col\">";
    headers+=titles[i];
    headers+="</th>";
  }
  return headers;
}

function createSummaryTable (headers, list) {
  let table=document.getElementById("summary");
  table.innerHTML = headers+list;
}

function sortByCriteria (members, criteria, order) {
	if (order==="asc") {   
		members.sort(function(a,b){return a[criteria]-b[criteria]});
	} else if (order==="desc") {   
		members.sort(function(a,b){return b[criteria]-a[criteria]});
	}
	return members;
}

function filterByPercent (array, criteria, perc) {
	let filteredList = [];  
  for (let i=0;(i<array.length*perc-1)||(array[i][criteria]==array[i-1][criteria]);i++) {
    filteredList.push(array[i]);
  }
	return filteredList;
}

function createDataHeaders (criteria) {
  let titles = ["Name"];
  if (criteria == "votes_with_party_pct") {
    titles.push("No. Party Votes");
    titles.push("% Party Votes");
  } else if (criteria == "missed_votes_pct") {
    titles.push("No. Missed Votes");
    titles.push("% Missed Votes");
  }
  return createHeaders(titles);
}

function createDataString (array, criteria) {
	let tableString="";
	for (let i=0; i<array.length; i++) {
        tableString+="<tr>";		
		tableString+="<td>";
		tableString+=array[i].last_name;
		tableString+=", ";
		tableString+=array[i].first_name;
		tableString+=" ";
		tableString+=(array[i].middle_name || "");
		tableString+="(";
		tableString+=array[i].party;
		tableString+=")";
		tableString+="</td>"
    if (criteria == "votes_with_party_pct") {     
      tableString+="<td>";
      tableString+=Math.round(array[i].total_votes*array[i][criteria]/100);
      tableString+="</td>";    
      tableString+="<td>";
      tableString+=array[i][criteria];
      tableString+=" %";
      tableString+="</td>";
      tableString+="</tr>";
    } else if (criteria == "missed_votes_pct") {     
      tableString+="<td>";
      tableString+=array[i].missed_votes;
      tableString+="</td>";      
      tableString+="<td>";
      tableString+=array[i][criteria];
      tableString+=" %";
      tableString+="</td>";
      tableString+="</tr>";
    }
	}

	return tableString;
}

function updateDataNamesLoyalty (members) {
  let string="";
  for (let i=0;i<members.length;i++) {
    string=""
    string+=members[i].last_name;
    string+=", ";
    string+=members[i].first_name;
    string+=" ";
    if(members[i].middle_name) {
      string+=members[i].middle_name+" ";
    }
    string+="("+members[i].party+")";
    members[i].name=string;
    delete members[i].last_name;
    delete members[i].middle_name;
    delete members[i].first_name;
    delete members[i].url;
    delete members[i].party;
  }
  return members;
}

function createTables (headers, list, id) {
	let table=document.getElementById(id);
	table.innerHTML = headers+list;
}

function createAllTables () { 
  let appVariableNameA = "";
  let appVariableNameB = "";
  let criteria = "";
  let idAsc = "";
  let idDesc = "";
  let perc = 0.1;
  if (document.getElementById("least-loyal")) {
    criteria = "votes_with_party_pct";    
    idAsc = "least-loyal";
    idDesc = "most-loyal";

    applifyData (
      leastLoyalApp,
      updateDataNamesLoyalty(
        updateLoyaltyData(
          createMembersData(
            filterByPercent(
              sortByCriteria(
                duplicateMembers(),
                criteria,
                "asc"),
              criteria,
              perc),
            loyaltyJSON))),
      loyaltyKeys,
      loyaltyHeaders);

    applifyData (
      mostLoyalApp,
      updateDataNamesLoyalty(
        updateLoyaltyData(
          createMembersData(
            filterByPercent(
              sortByCriteria(
                duplicateMembers(),
                criteria,
                "desc"),
              criteria,
              perc),
            loyaltyJSON))),
      loyaltyKeys,
      loyaltyHeaders);

  } else if (document.getElementById("least-engaged")) {
    criteria = "missed_votes_pct";   
    idAsc = "most-engaged";
    idDesc = "least-engaged";

    applifyData (
      leastEngagedApp,
      updateDataNamesLoyalty(
        createMembersData(
          filterByPercent(
            sortByCriteria(
              duplicateMembers(),
              criteria,
              "desc"),
            criteria,
            perc),
          attendanceJSON)),
      attendanceKeys,
      attendanceHeaders);

    applifyData (
      mostEngagedApp,
      updateDataNamesLoyalty(
        createMembersData(
          filterByPercent(
            sortByCriteria(
              duplicateMembers(),
              criteria,
              "asc"),
            criteria,
            perc),
          attendanceJSON)),
      attendanceKeys,
      attendanceHeaders);

  }
}
