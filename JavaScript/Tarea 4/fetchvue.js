var data = {};

var url;

if (document.getElementById("senate")) {
  url = 'https://api.propublica.org/congress/v1/113/senate/members.json';
} else if (document.getElementById("house")) {
  url = 'https://api.propublica.org/congress/v1/113/house/members.json';
}

$(function() {
  var petitionHeaders = new Headers();
  petitionHeaders.append('X-API-Key', 'aJryiKkbvThq91sdQ0kNK44HhaAyiQoWGIkbs45k');
  var init = {
    method: 'GET',
    headers: petitionHeaders
  }

  fetch(url, init)
    .then(function(response){
      if (response.ok) {
        data = response.json().then(function (result) {
          data = result;
          tablifyData();
        });
      } else {
        console.log("Error: "+response.status);
      }
    }).catch(function(error){
      console.log("Fallo: "+error.message);
    });
});

function tablifyData () {
  if (document.getElementById("data-table")) {
    updateList();
  } else if (document.getElementById("most-engaged") || document.getElementById("most-loyal")) {
    createStatistics();
    createAllTables();
  }
}
