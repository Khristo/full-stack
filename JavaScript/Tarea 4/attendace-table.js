function tables(array, table){
        tabla = document.getElementById(table);
        tblBody = document.createElement("tbody");

        for(j = 0; j < array.length; j++){
            tr = document.createElement("tr");
            fullname = array[j].first_name + " " + array[j].last_name;
            numberPartyVotes = array[j].missed_votes;
            pctVotes = array[j].missed_votes_pct;

            nameNode = document.createTextNode(fullname);
            numberNode = document.createTextNode(numberPartyVotes);
            votesNode  = document.createTextNode(pctVotes);

            td = document.createElement("td");
            td.appendChild(nameNode);
            tr.appendChild(td);
            td = document.createElement("td");
            td.appendChild(numberNode);
            tr.appendChild(td);
            td = document.createElement("td");
            td.appendChild(votesNode);
            tr.appendChild(td);
            tblBody.appendChild(tr);
        }
        tabla.appendChild(tblBody);
    }
    tables(stadistics.resultados[0].leastEngaged[0], "least-engaged-table");
    tables(stadistics.resultados[0].mostEngaged[0], "most-engaged-table");
