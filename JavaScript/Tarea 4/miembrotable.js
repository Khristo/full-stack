var dataHeaders = [
  "Name",
  "Party",
  "State",
  "Seniority",
  "% votes w/ party"
];

var dataKeys = [
  "name",
  "party",
  "state",
  "seniority",
  "votes_with_party_pct"
];

var dataApp = new Vue({
  el: "#data-table",
  data: {
    members:"",
    headers:""
  }
});

var dataJSON = {
  "first_name":"",
  "last_name":"",
  "middle_name":"",
  "url":"",
  "party":"",
  "state":"",
  "seniority":"",
  "votes_with_party_pct":""
};

function applifyData (appVariable, appData, appKeys, appHeaders) {
  appVariable.members=appData;
  appVariable.keys=appKeys;
  appVariable.headers=appHeaders;
}

function createMembersData (membersData, json) {
  let duplicatedJSON = {};
  let newMembersList = [];
  
  let keys = [];
  keys=Object.keys(json);
  for (let i=0;i<membersData.length;i++) {
    duplicatedJSON = json;
    for (let j=0;j<keys.length;j++) {
      duplicatedJSON[keys[j]]=membersData[i][keys[j]];
    }
    newMembersList.push(JSON.parse(JSON.stringify(duplicatedJSON)));
  }
  return newMembersList;
}

function updateDataNames (members) {
  let string="";
  for (let i=0;i<members.length;i++) {
    string=""
    if (members[i].url != "") {
    string+="<a href=\"";
      string+=members[i].url;
      string+="\">";
    }
    string+=members[i].last_name;
    string+=", ";
    string+=members[i].first_name;
    string+=" ";
    string+=(members[i].middle_name || "");
    if (members[i].url != "") {
      string+="</a>";
    }
    members[i].name=string;
    delete members[i].last_name;
    delete members[i].middle_name;
    delete members[i].first_name;
    delete members[i].url;
  }
  return members;
}

function filterArrayOfMembers (array, state, parties) {
  let filteredArray = [];
  let partiesToFilter = parties;
  let stateString = "";
  for (let i=0;i<array.length;i++) {
    stateString = ((array[i].state)+("_state")).toLowerCase();
    if (state == "every_state" || stateString == state) {
      for (let j=0;j<partiesToFilter.length;j++) {
        if (array[i].party == partiesToFilter[j]) {
          filteredArray.push(array[i]);
        }
      }
    }
  }
  return filteredArray;
}

function getPartyFilter() {
  let partiesList = [];
  if (document.getElementById("party_republican").checked==true) {
    partiesList.push("R");
  }
  if (document.getElementById("party_democrat").checked==true) {
    partiesList.push("D");
  }
  if (document.getElementById("party_independent").checked==true) {
    partiesList.push("I");
  }
  return partiesList;
}

function getStateFilter() {
  let state = document.getElementById("state_select").value;
  return state;
}

function createList (array) {
  let tableString="";
  for (let i=0;i<array.length;i++) {
        tableString+="<tr>";  
        tableString+="<td>";       
        if (array[i].url != "") {
          tableString+="<a href=\"";
          tableString+=array[i].url;
          tableString+="\">";
        }
        tableString+=array[i].last_name;
        tableString+=", ";
        tableString+=array[i].first_name;
        tableString+=" ";
        tableString+=(array[i].middle_name || "");
        if (array[i].url != "") {
          tableString+="</a>";
        }
        tableString+="</td>"     
        tableString+="<td>";
        tableString+=array[i].party;
        tableString+="</td>"      
        tableString+="<td>";
        tableString+=array[i].state;
        tableString+="</td>"      
        tableString+="<td>";
        tableString+=array[i].seniority;
        tableString+="</td>"      
        tableString+="<td>";
        tableString+=array[i].votes_with_party_pct;
        tableString+=" %"
        tableString+="</td>"
        tableString+="</tr>"
  }
  return tableString;
}

function createHeaders () {
  let headers="";
      headers+="<tr>";
      headers+="<th scope=\"col\">";
      headers+="Name";
      headers+="</th>";
      headers+="<th scope=\"col\">";
      headers+="Party";
      headers+="</th>";    
      headers+="<th scope=\"col\">";
      headers+="State";
      headers+="</th>";   
      headers+="<th scope=\"col\">";
      headers+="Seniority";
      headers+="</th>";   
      headers+="<th scope=\"col\">";
      headers+="% votes w/ party";
      headers+="</th>";
      headers+="</tr>";
  return headers;
}

function createTable (headers, list) {
  let table=document.getElementById("data-table");
  if (list.length < 1) {
    list = ["<td colspan=\"5\" style=\"text-align:center;\"><strong>No matches found.</strong></td>"];
  }
  table.innerHTML = headers+list;
}

function duplicateMembers () {
  let duplicatedMembers = data.results[0].members.slice(0);
  return duplicatedMembers;
}

function updateList () {
  applifyData(
    dataApp,
    updateDataNames
      (
      createMembersData
        (
        filterArrayOfMembers
          (
          duplicateMembers(),
          getStateFilter(getStateFilter),
          getPartyFilter(getPartyFilter)
          ),
        dataJSON
        )
      ),
    dataKeys,
    dataHeaders
  );
}
