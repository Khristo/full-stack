var stadistics = {
        "resultados": [{
            "partido":[{
                    "miembros" : [],
                    "party" : "D",
                    "pct_votos" : 0,
                    "total_miembros" : 0,
                    "votos_partido" : 0
                },
                {
                    "miembros" : [],
                    "party" : "R",
                    "pct_votos" : 0,
                    "total_miembros" : 0,
                    "votos_partido" : 0
                },
                {
                    "miembros" : [],
                    "party" : "I",
                    "pct_votos" : 0,
                    "total_miembros" : 0,
                    "votos_partido" : 0
                }],
            "leastLoyal" : [],
            "mostLoyal" : [],
            "leastEngaged" : [],
            "mostEngaged" : []
        }]
    }
    
var datos = dataSenate.results[0].members,
    pctTabla = dataSenate.results[0].members.length / 10;

function numerosMiembros(){
    datos.forEach(element => {
        if(element.party === "D"){
            stadistics.resultados[0].partido[0].miembros.push(element);
        }else if(element.party === "R"){
            stadistics.resultados[0].partido[1].miembros.push(element);
        }else if(element.party === "I"){
            stadistics.resultados[0].partido[2].miembros.push(element);
        }
    }); 
}
numerosMiembros();

function porcentajeVotos(){
    
    partidoVotos = stadistics.resultados[0].partido;
    
    partidoVotos.forEach( element => {
        totalVotosPorPartido = 0;
        for(j= 0; j < element.miembros.length; j++){
            totalVotosPorPartido += element.miembros[j].votes_with_party_pct;
        }
        porcentaje = totalVotosPorPartido / element.miembros.length;
        element.pct_votos = porcentaje.toFixed(2); 
    });
}
porcentajeVotos();


function loyalty(){
    
    function sort(a,b){
        a = a.votes_with_party_pct,
        b = b.votes_with_party_pct;
        if(a < b){
            return 1;
        }else if(a > b){
            return -1;
        }
        return 0;
    }
    datos.sort(sort);

     
    var arrayTop = Math.round(datos.length * 0.1),
        topTen = datos.slice(0, arrayTop);
    
    function mLoyal(mostLoyal){
            if(datos[arrayTop].votes_with_party_pct == datos[arrayTop + 1].votes_with_party_pct){
                topTen = datos.slice(0, arrayTop + 1);
                mostLoyal.push(topTen);
            }else{
                mostLoyal.push(topTen);
            }
    }
    mLoyal(stadistics.resultados[0].mostLoyal);
    
    
    function lLoyal(leastLoyal){
        reverse = datos.reverse();
        topTen = reverse.slice(0, arrayTop);
    
        if(reverse[arrayTop].votes_with_party_pct == reverse[arrayTop + 1].votes_with_party_pct){
            topTen = reverse.slice(0, arrayTop + 1);
            leastLoyal.push(topTen);
        }else{
            leastLoyal.push(topTen);
        }
    }
    lLoyal(stadistics.resultados[0].leastLoyal);
}
loyalty();

//Attendance
function attendance(){
    //Ordena el array
    function sort(a,b){
        a = a.missed_votes_pct,
        b = b.missed_votes_pct;
        if(a < b){
            return 1;
        }else if(a > b){
            return -1;
        }
        return 0;
    }
    
    datos.sort(sort);
    
    //leastEngaged 
    var arrayTop = Math.round(datos.length * 0.1),
        topTen = datos.slice(0, arrayTop);
    
    function lEngaged(leastEngaged){
            if(datos[arrayTop].missed_votes_pct == datos[arrayTop + 1].missed_votes_pct){
                topTen = datos.slice(0, arrayTop + 1);
                leastEngaged.push(topTen);
            }else{
                leastEngaged.push(topTen);
            }
    }
    lEngaged(stadistics.resultados[0].leastEngaged);
    
    //MostEngaged
    function mEngaged(mostEngaged){
        reverse = datos.reverse();
        topTen = reverse.slice(0, arrayTop);
    
        if(reverse[arrayTop].missed_votes_pct == reverse[arrayTop + 1].missed_votes_pct){
            topTen = reverse.slice(0, arrayTop + 1);
            mostEngaged.push(topTen);
        }else{
            mostEngaged.push(topTen);
        }
    }
    mEngaged(stadistics.resultados[0].mostEngaged);
}
attendance();
