var myName = "Kevin";
var age = 26;
var ignasiAge = 32;
var ageDiff= age-ignasiAge;

if (age>21) {
 console.log("es mayor de 21");
} else {
  console.log("es menor de 21");
}

console.log (myName,age,ageDiff);

if (age<ignasiAge) {
  console.log ("Ignasi es mayor que usted");  
} else {
  if (age>ignasiAge) {
    console.log ("Ignasi es más joven que usted");
  } else {
    console.log ("Tiene la misma edad que Ignasi");
  }
}

var nameClass = ['Carla','Caro','Christian','Cristian','Damian','Daniel','David','Diana','Ecoraita','Evelyn','Florencia'
                ,'Freddy','Jesus','Juan','JuanMa','Lau','Mariano','Micaela','Nadia','Natalia','Nicolas G','Nicolas M','Rocio',
                 'Santiago','Satock33','Sebastian','Sherly','Silvia','Sol','Ulises'];

nameClass.sort();

console.log(nameClass[0],nameClass[29]);

for (var i = 0; i < nameClass.length; ++i) {
    console.log(nameClass[i]);
  }

console.log('-------------------------------------------------------')

var edadClass = [58,52,38,26,30,59,42,43,56,32,37,53,39,60,24,41,20,54,55,40,21,45,25,22,57,27,33,28,31,48];

var n=0;
while(n<edadClass.length) {
  if (edadClass[n]%2===0){
        console.log(edadClass[n]);
  }
  n++;
}
n=0;
console.log('----------------------------------------------------')

i=0;
for (i = 0; i < edadClass.length; i++) {
  if (edadClass[i]%2===0){
        console.log(edadClass[i]);
  }
}

console.log('----------------------------------------------------')

function elMasBajo(a) {
  var min=a[0];
  for (var j = 0; j < a.length; j++) {
    if (min>a[j]) {
       min=a[j];
    }
  }
  return min;
}

console.log(elMasBajo(edadClass));

function elMasGrande(a) {
  var max = a[0];
  for (var j = 0; j < a.length; j++) {
    if (max<a[j]) {
       max=a[j];
    }
  }
  return max;
}

console.log(elMasGrande(edadClass));

function valorPosicion(a,b) {
  return a[b];
}

console.log(valorPosicion(edadClass,2));

var numerosrepe = [3,6,67,6,23,11,100,8,93,0,17,24,7,1,33,45,28,33,23,12,99,100];

function iguales(arr) {
  var duplicado = [];
  for (var i = 0; i < arr.length; i++) {
    for (var j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j] && duplicado.indexOf(arr[j]) === -1) {
        duplicado.push(arr[j]);

      }
    }
  }

return duplicado;

}

console.log(iguales(numerosrepe));

var myColor = ["Red", "Green", "White", "Black"];

function unir(a) {
  return a.join();
}

console.log (unir(myColor));

console.log ('------------------------------------------------------')

function invertir(a) {
	 a = a + "";
	 return a.split("").reverse().join("");
  }

console.log(invertir(32243));

function ordenAlfabetico(a) {
	 return a.split('').sort().join('');
  }

console.log(ordenAlfabetico("webmaster"));

function mayuscula(a) {
  var array = a.split(' ');
  var newarray = [];    
  for(var x = 0; x < array.length; x++){
      newarray.push(array[x].charAt(0).toUpperCase()+array[x].slice(1));
  }
  return newarray.join(' ');
}

console.log (mayuscula("prince of persia"));

function palabraLarga(a) {
  var array = a.match(/\w[a-z]{0,}/gi);
  var larga = array[0];

  for(var x = 1 ; x < array.length ; x++)
  {
    if(larga.length < array[x].length)
    {
    larga = array[x];
    } 
  }
  return larga;
}
console.log(palabraLarga('Web Development Tutorial'));