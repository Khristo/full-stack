var statistics = {
  parties:[{
    "name":"Democrats",
    "number_of_members":0,
    "perc_voted_with_party":0
  },{
    "name":"Republicans",
    "number_of_members":0,
    "perc_voted_with_party":0
  },{
    "name":"Independents",
    "number_of_members":0,
    "perc_voted_with_party":0
  },{
    "name":"Total",
    "number_of_members":0,
    "perc_voted_with_party":0
  }]
}

let democratList = [];
let republicanList = [];
let independentList = [];

var summaryApp = new Vue({
  el: "#summary-table",
  data: {
    members:"",
    headers:""
  }
});

var summaryHeaders = ["Party","No. of Reps.","% Voted w/ Party"];

var summaryKeys = ["name","number_of_members","perc_voted_with_party"];

function createPartiesLists (members) {
	for (let i = 0; i<members.length; i++) {
		switch (members[i].party) {
			case "R":
				republicanList.push(members[i]);
				break;
			case "D":
				democratList.push(members[i]);
				break;
			case "I":
				independentList.push(members[i]);
				break;
			default:;
		}
	}
}

function updateMembersQty () {
	statistics.parties[0].number_of_members=democratList.length;
	statistics.parties[1].number_of_members=republicanList.length;
	statistics.parties[2].number_of_members=independentList.length;
	statistics.parties[3].number_of_members=data.results[0].members.length;
}

function updatePartyLoyalty () {
	statistics.parties[0].perc_voted_with_party=arrayAverager(democratList);
	statistics.parties[1].perc_voted_with_party=arrayAverager(republicanList);
	statistics.parties[2].perc_voted_with_party=arrayAverager(independentList);
	statistics.parties[3].perc_voted_with_party=arrayAverager(data.results[0].members);
}


function arrayAverager (array) {
  let accum=0;
  if (array.length==0) {
    accum="-";
  } else {
    for (let i=0; i<array.length; i++) {
      accum+=array[i].votes_with_party_pct;
    }
    accum/=array.length;
    accum=parseFloat(accum.toFixed(2));
    accum=""+accum;
  }
	return accum;
}

function createStatistics () {
  createPartiesLists(duplicateMembers());
  updateMembersQty();
  updatePartyLoyalty();
  applifyData (
    summaryApp,
    statistics.parties,
    summaryKeys,
    summaryHeaders
  );
}
