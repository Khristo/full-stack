var dataHeaders = [
  "Name",
  "Party",
  "State",
  "Seniority",
  "% votes w/ party"
];

var dataKeys = [
  "name",
  "party",
  "state",
  "seniority",
  "votes_with_party_pct"
];

var dataApp = new Vue({
  el: "#data-table",
  data: {
    members:"",
    headers:""
  }
});

function createSenateData (membersData) {
  let senateData = [];
  let senateJSON = {
    "first_name":"",
    "last_name":"",
    "middle_name":"",
    "url":"",
    "party":"",
    "state":"",
    "seniority":"",
    "votes_with_party_pct":""
  };
  
  let keys = [];
  keys=Object.keys(senateJSON);
  for (let i=0;i<membersData.length;i++) {
    senateJSON = {
      "first_name":"",
      "last_name":"",
      "middle_name":"",
      "url":"",
      "party":"",
      "state":"",
      "seniority":"",
      "votes_with_party_pct":""
    };
    for (let j=0;j<keys.length;j++) {
      senateJSON[keys[j]]=membersData[i][keys[j]];
    }
    senateData.push(JSON.parse(JSON.stringify(updateName(senateJSON))));
  }
  return senateData;
}

function applifyMembers (appVariable, appMembers,appKeys,appHeaders) {
  appVariable.members=appMembers;
  appVariable.keys=appKeys;
  appVariable.headers=appHeaders;
}

function updateName (member) {
  let string="";
  if (member.url != "") {

  string+="<a href=\"";
    string+=member.url;
    string+="\">";
  }
  string+=member.last_name;
  string+=", ";
  string+=member.first_name;
  string+=" ";
  string+=(member.middle_name || "");
  if (member.url != "") {
    string+="</a>";
  }
  member.name=string;
  delete member.last_name;
  delete member.middle_name;
  delete member.first_name;
  delete member.url;
  return member;
}

function filterArrayOfMembers (array, state, parties) {
  let filteredArray = [];
  let partiesToFilter = parties;
  let stateString = "";
  for (let i=0;i<array.length;i++) {
    stateString = ((array[i].state)+("_state")).toLowerCase();
    if (state == "every_state" || stateString == state) {
      for (let j=0;j<partiesToFilter.length;j++) {
        if (array[i].party == partiesToFilter[j]) {
          filteredArray.push(array[i]);
        }
      }
    }
  }
  return filteredArray;
}

function getPartyFilter() {
  let partiesList = [];
  if (document.getElementById("party_republican").checked==true) {
    partiesList.push("R");
  }
  if (document.getElementById("party_democrat").checked==true) {
    partiesList.push("D");
  }
  if (document.getElementById("party_independent").checked==true) {
    partiesList.push("I");
  }
  return partiesList;
}

function getStateFilter() {
  let state = document.getElementById("state_select").value;
  return state;
}

function createList (array) {
  let tableString="";
  for (let i=0;i<array.length;i++) {
    tableString+="<tr>";      
    tableString+="<td>";       
    if (array[i].url != "") {
      tableString+="<a href=\"";
      tableString+=array[i].url;
      tableString+="\">";
    }
    tableString+=array[i].last_name;
    tableString+=", ";
    tableString+=array[i].first_name;
    tableString+=" ";
    tableString+=(array[i].middle_name || "");
    if (array[i].url != "") {
      tableString+="</a>";
    }
    tableString+="</td>"     
    tableString+="<td>";
    tableString+=array[i].party;
    tableString+="</td>"    
    tableString+="<td>";
    tableString+=array[i].state;
    tableString+="</td>"     
    tableString+="<td>";
    tableString+=array[i].seniority;
    tableString+="</td>"     
    tableString+="<td>";
    tableString+=array[i].votes_with_party_pct;
    tableString+=" %"
    tableString+="</td>"
    tableString+="</tr>"
  }
  return tableString;
}

function createHeaders () {
  let headers="";
    headers+="<tr>";
    headers+="<th scope=\"col\">";
    headers+="Name";
    headers+="</th>";
    headers+="<th scope=\"col\">";
    headers+="Party";
    headers+="</th>";
    headers+="<th scope=\"col\">";
    headers+="State";
    headers+="</th>";
    headers+="<th scope=\"col\">";
    headers+="Seniority";
    headers+="</th>";
    headers+="<th scope=\"col\">";
    headers+="% votes w/ party";
    headers+="</th>";
    headers+="</tr>";
  return headers;
}

function createTable (headers, list) {
  let table=document.getElementById("data-table");
  if (list.length < 1) {
    list = ["<td colspan=\"5\" style=\"text-align:center;\"><strong>No matches found.</strong></td>"];
  }
  table.innerHTML = headers+list;
}

function duplicateMembers () {
  let duplicatedMembers = data.results[0].members.slice(0);
  return duplicatedMembers;
}

function updateList () {
  applifyMembers(
    dataApp,
    createSenateData(
      filterArrayOfMembers(
        duplicateMembers(),
        getStateFilter(getStateFilter),
        getPartyFilter(getPartyFilter)
      )
    ),
    dataKeys,
    dataHeaders
  );  
}
