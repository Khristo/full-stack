function tables(array, table){
    tabla = document.getElementById(table);
    tblBody = document.createElement("tbody");

    for(j = 0; j < array.length; j++){
        tr = document.createElement("tr");
        fullname = array[j].first_name + " " + array[j].last_name;
        numberPartyVotes = array[j].total_votes;
        pctVotes = array[j].votes_with_party_pct;

        nameNode = document.createTextNode(fullname);
        numberNode = document.createTextNode(numberPartyVotes);
        votesNode  = document.createTextNode(pctVotes);

        td = document.createElement("td");
        td.appendChild(nameNode);
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(numberNode);
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(votesNode);
        tr.appendChild(td);
        tblBody.appendChild(tr);
    }
    tabla.appendChild(tblBody);
}
tables(stadistics.resultados[0].leastLoyal[0], "least-loyal-table");
tables(stadistics.resultados[0].mostLoyal[0], "most-loyal-table");
